package com.accenture.banking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="client")
public class Client {

	@Id
	private int id;
	private String name;
	private String surname;
	@Column(name="created_datetime")
	private String createdDatetime;
	
	@Column(name="deleted_datetime")
	private String deletedDatetime;
	
	public Client() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(String createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDeletedDatetime() {
		return deletedDatetime;
	}

	public void setDeletedDatetime(String deletedDatetime) {
		this.deletedDatetime = deletedDatetime;
	}
	
}
