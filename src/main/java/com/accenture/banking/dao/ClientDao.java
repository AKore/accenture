package com.accenture.banking.dao;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.accenture.banking.model.Client;


public interface ClientDao extends JpaRepository<Client, Long> {
	
	//TODO Crear cada implementación según interese
	List<Client> findById(int id);
	
//	@Query("select t from  Titular t where nombre=?1")
//	Client getTitular(String nombre);
	
	
}
