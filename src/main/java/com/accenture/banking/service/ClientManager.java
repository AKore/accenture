package com.accenture.banking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.banking.dao.ClientDao;
import com.accenture.banking.model.Client;


@Service
public class ClientManager {
	
	@Autowired
	private ClientDao clientDao;
	
	
	
	public Client searchClient(String titular){
		Client t = null;//clientDao.getTitular(titular);
		return t;
	}
	
	public void saveClient(Client t){
		clientDao.save(t);
	}
	
	public void saveClients(List<Client> titulares){
		clientDao.save(titulares);
	}
	
	public List<Client> getAllClients(){
		return clientDao.findAll();
	}
}
