package com.accenture.banking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accenture.banking.model.Client;
import com.accenture.banking.service.AccountManager;
import com.accenture.banking.service.OfficeManager;
import com.accenture.banking.service.ClientManager;

@Controller
public class BankingController {

	@Autowired
	private AccountManager accountManager;
	
	@Autowired
	private ClientManager clientManager;
	
	@Autowired
	private OfficeManager officeManager;
	
	@ResponseBody
	@RequestMapping("/getClient")
	private Client getTitular(@RequestParam("titular") String titular){
		Client t = clientManager.searchClient(titular);
		return t;
	}
	
	@ResponseBody
	@RequestMapping("/getClients")
	private List<Client> qwkjeqwkjeb(){
		List<Client> t = clientManager.getAllClients();
		return t;
	}
}
