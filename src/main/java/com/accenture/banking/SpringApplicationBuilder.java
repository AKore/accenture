package com.accenture.banking;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

import com.accenture.banking.model.Client;
import com.accenture.banking.service.ClientManager;


@SpringBootApplication @PropertySource("classpath:application.properties")
public class SpringApplicationBuilder implements CommandLineRunner {

	@Autowired
	private ClientManager clientManager;

	public static void main(String[] args) {
		SpringApplication.run(SpringApplicationBuilder.class, args);
		
	}

	@Override
	public void run(String... arg0) throws Exception {
		
		List <Client> t = clientManager.getAllClients();
		
		for (Client e: t){
			System.out.println(t.toString());
		}	
	}
}
